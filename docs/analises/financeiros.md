# Lançamentos (Financeiros)

A área de lançamentos financeiros é dividida em duas abas: lançamentos provisionados e lançamentos baixados. Para acessar quaisquer dessas opções, basta navegar entre as abas correspondentes:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/abas.JPG" />
</figure>

## Visualizando lançamentos provisionados

Tendo selecionado a opção de lançamentos provisionados, a seguinte tela será exibida:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/provisionados/painel.JPG" />
</figure>

A tabela de dados apresenta as seguintes informações:

`Código`
: Número de identificação do lançamento.

`Empresa`
: Empresa a qual o lançamento está ligado.

`Unidade`
: Unidade a qual o lançamento está ligado.

`Departamento`
: Departamento a qual o lançamento está ligado.

`Centro Custo`
: Centro de custos a qual o lançamento está ligado.

`Tipo`
: Tipo de lançamento, podendo ser:

    * Ingresso
    * Desembolso

`Plano de Contas`
: Classificação do lançamento dentro do plano de contas.

`Valor Previsto`
: Valor previsto do lançamento.

`Anexos`
: Quantidade de documentos anexados.

`Vencimento`
: Data de vencimento do lançamento.

`Lançado por`
: Informa quem realizou o lançamento.

`Lançado em`
: Informa data e hora da realização do lançamento.

`Situação`
: Situação do lançamento, podendo ser:

    * Analisado
    * Pendente
    * Baixado
    * Desativado

`Visualizar`
: Permite visualizar os dados do lançamento. Caso desejado, é possível editar os dados nesta mesma tela, e, para salvar as alterações, basta clicar em **alterar registro**. Nesta tela, também é possível ver se há algum lançamento baixado vinculado ao provisionado clicando na aba correspondente.

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/provisionados/visualizando-lancamento.JPG" />
</figure>

Caso desejado, é possível filtrar os dados por empresa, data e status na área correspondente:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/provisionados/filtro.JPG" />
</figure>

## Visualizando lançamentos baixados

Tendo selecionado a opção de lançamentos baixados, a seguinte tela será exibida:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/baixados/painel.JPG" />
</figure>

A tabela de dados apresenta as seguintes informações:

`Código`
: Número de identificação do lançamento.

`Empresa`
: Empresa a qual o lançamento está ligado.

`Unidade`
: Unidade a qual o lançamento está ligado.

`Departamento`
: Departamento a qual o lançamento está ligado.

`Centro de Custo`
: Centro de custos a qual o lançamento está ligado.

`Tipo`
: Tipo de lançamento, podendo ser:

    * Ingresso
    * Desembolso

`Plano de Contas`
: Classificação do lançamento dentro do plano de contas.

`Valor`
: Valor pago relacionado ao lançamento.

`Data Baixa`
: Data em que a baixa do lançamento foi realizada.

`Anexos`
: Quantidade de documentos anexados.

`Situação`
: Situação do lançamento, podendo ser:

    * Analisado
    * Pendente
    * Baixado
    * Desativado

`Visualizar`
: Permite visualizar os dados do lançamento. Caso desejado, é possível editar os dados nesta mesma tela, e, para salvar as alterações, basta clicar em **alterar registro**. Nesta tela, também é possível ver se há algum lançamento provisionado vinculado ao baixado clicando na aba correspondente.

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/baixados/visualizando-lancamento.JPG" />
</figure>

Caso desejado, é possível filtrar os dados por empresa, data e status na área correspondente:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/provisionados/filtro.JPG" />
</figure>

## Realizando lançamento financeiro

Para realizar um novo lançamento, é necessária a realização das seguintes etapas:

1. Clicar no botão **novo lançamento**:

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/novo-botao.JPG" />
</figure>

<ol start="2">
  <li>Inserir os dados para o novo lançamento, e, então, clicar em <strong>salvar</strong>.</li>
</ol>

<figure class="images">
    <img src="../../assets/images/lancamentos/financeiros/novo.JPG" />
</figure>