# Fluxo de Caixa Mensal

Essa área possibilita a visualização do fluxo de caixa mensal de uma empresa. A tela inicial é assim:

<figure class="images">
    <img src="../../assets/images/fluxo-caixa-painel.JPG" />
</figure>

## Visualizando o fluxo de caixa mensal

Para visualizar o fluxo de caixa mensal, é necessária a realização dos seguintes passos:

1. Filtrar os dados que se queira ver, através do filtro:

<figure class="images">
    <img src="../../assets/images/fluxo-caixa-filtro.JPG" />
</figure>

<ol start="2">
  <li>Após a seleção, os dados aparecerão de forma silimar a apresentada a seguir. A visualização pode ser feita de forma sintética (resumida) ou analítica (expandida).</li>
</ol>

<figure class="images">
    <img src="../../assets/images/fluxo-caixa-painel-gerado.JPG" />
</figure>